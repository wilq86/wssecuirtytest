﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WSSecurityTest
{
    public sealed class WsSecurityMessageInspector : IClientMessageInspector
    {
        public const string BodyIdentifier = "ws-security-body-id"; // This can be whatever xml Id attribute value value we want

        public X509Certificate2 X509Certificate { get; }

        public WsSecurityMessageInspector() { }

        public WsSecurityMessageInspector(X509Certificate2 cert)
        {
            X509Certificate = cert;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState) { }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            // Add the ws-Security header
            //request.Headers.Add(new WsSecurityHeader());

            //// Get the entire message as an xml doc, so we can sign the body.
            var xml = GetMessageAsString(request);

            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.LoadXml(xml);

            var newDoc = new WsSecuritySign().SignXml(doc, X509Certificate);

            var newMessage = CreateMessageFromXmlDocument(request, newDoc);

            request = newMessage;
            return null;

            //XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            //nsmgr.AddNamespace("soapenv", WsSecurityHeader.SoapEnvelopeNamespace);
            //nsmgr.AddNamespace("wsse", WsSecurityHeader.WsseNamespace);

            //// The Body is the element we want to sign.
            //var body = doc.SelectSingleNode("//soapenv:Body", nsmgr) as XmlElement;

            //// Add the Id attribute to the Body, for the Reference element URI..
            //var id = doc.CreateAttribute("wsu", "Id", WsSecurityHeader.WsseUtilityNamespaceUrl);
            //id.Value = BodyIdentifier;
            //body.Attributes.Append(id);

            //// Here we do not adopt the SecurityTokenReference recommendation in the KeyInfo
            //// section because it is not defined in the XML Signature standard. In lieu of the SecurityTokenReference, we
            //// add KeyInfoX509Data directly to the KeyInfo node, in accordance with the XML Signature rfc (rfc3075).  The SignedXml
            //// class does not seem to support the SecurityTokenReference, and it's not required.
            //var signedXml = new SignedXmlWithUriFix(doc);
            //signedXml.SignedInfo.SignatureMethod = SignedXml.XmlDsigRSASHA1Url;

            //// This cannonicalization method is "recommended" in the ws-security standard, but seems to be required, at least
            //// by Data Power. 
            //signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;

            //// Add the X509 certificate info to the KeyInfo section
            //var keyInfo = new KeyInfo();
            //var keyInfoData = new KeyInfoX509Data();

            //keyInfoData.AddIssuerSerial(X509Certificate.IssuerName.Name, X509Certificate.SerialNumber);
            //keyInfo.AddClause(keyInfoData);

            //signedXml.SigningKey = X509Certificate.PrivateKey;
            //signedXml.KeyInfo = keyInfo;

            //// Add the reference to the SignedXml object.
            //Reference reference = new Reference($"#{BodyIdentifier}");
            //reference.DigestMethod = SignedXml.XmlDsigSHA1Url;

            //signedXml.AddReference(reference);

            //// Compute the signature.
            //signedXml.ComputeSignature();

            //// Get the Signature element
            //XmlElement xmlDigitalSignature = signedXml.GetXml();

            //// Append the Signature element to the XML document's Security header.
            //XmlNode header = doc.SelectSingleNode("//soapenv:Envelope/soapenv:Header/wsse:Security", nsmgr);
            //header.AppendChild(doc.ImportNode(xmlDigitalSignature, true));

            //// Generate a new message from our XmlDocument.  We have to be careful here so that the XML is serialized 
            //// with the same whitespace handling (via XmlWriter) as the signed xml (via XmlDocument). A bit sketchy.
            //var newMessage = CreateMessageFromXmlDocument(request, doc);

            //request = newMessage;

            //return null;
        }

        private Message CreateMessageFromXmlDocument(Message message, XmlDocument doc)
        {
            MemoryStream ms = new MemoryStream();
            using (XmlWriter xmlWriter = XmlWriter.Create(ms, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = false }))
            {
                doc.WriteTo(xmlWriter);
                xmlWriter.Flush();
                xmlWriter.Close();
                ms.Position = 0;
            }
            XmlDictionaryReader xdr = XmlDictionaryReader.CreateTextReader(ms, new XmlDictionaryReaderQuotas());

            var newMessage = Message.CreateMessage(xdr, int.MaxValue, message.Version);

            newMessage.Properties.CopyProperties(message.Properties);

            return newMessage;
        }

        private string GetMessageAsString(Message msg)
        {
            using (var sw = new StringWriter())
            using (var xw = new XmlTextWriter(sw))
            {
                msg.WriteMessage(xw);
                return sw.ToString();
            }
        }

        /// <summary>
        /// The SignedXml class chokes on a URI prefixed with "#", so we override the GetIdElement here.  The #
        /// is allowed by the XML Signature rfc (rfc3075), so this is really a bug fix for SignedXml.
        /// </summary>
    }
}
