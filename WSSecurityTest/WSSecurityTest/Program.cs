﻿using ServiceReference1;
using System.Reflection.PortableExecutable;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel;
using System.Xml;
using WSSecurityTest;

var certificate = new X509Certificate2("apples.pfx", "apples");

var binding = new BasicHttpsBinding();
binding.Security.Mode = BasicHttpsSecurityMode.Transport;

var endPoint = new EndpointAddress("https://www.dneonline.com/calculator.asmx");

var client = new CalculatorSoapClient(binding, endPoint);

// Configure ws-security signing
client.ChannelFactory.Endpoint.EndpointBehaviors.Add(new WsSecurityHeaderBehavior(certificate));

var a = await client.AddAsync(1, 2);
