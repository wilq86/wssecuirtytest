﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WSSecurityTest
{
    internal class WsSecuritySign
    {
        public const string WSSE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        public const string DS = "http://www.w3.org/2000/09/xmldsig#";

        public XmlDocument SignXml(XmlDocument doc, X509Certificate2 cert)
        {
            XmlElement body = (XmlElement)doc.GetElementsByTagName("s:Body")[0];

            body.SetAttribute("wsu:Id", "id-280D57E3B163C0FD9C168440974461814");

            SignedXmlWithId signedXml = new SignedXmlWithId(doc);

            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;
            XmlDsigExcC14NTransform tranform = (XmlDsigExcC14NTransform)signedXml.SignedInfo.CanonicalizationMethodObject;
            tranform.InclusiveNamespacesPrefixList = "api soapenv";

            signedXml.SigningKey = cert.PrivateKey;
            //Required specific private key
            //signedXml.SignedInfo.SignatureMethod = SignedXml.XmlDsigSHA256Url;
            //signedXml.SignatureMethod = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

            Reference reference = new Reference();
            reference.Uri = "#id-280D57E3B163C0FD9C168440974461814";

            //XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            XmlDsigExcC14NTransform c14n = new XmlDsigExcC14NTransform();
            c14n.InclusiveNamespacesPrefixList = "api";

            //reference.AddTransform(env);
            reference.AddTransform(c14n);

            signedXml.AddReference(reference);


            signedXml.ComputeSignature();

            //XmlElement signature = signedXml.GetXml();
            //doc.DocumentElement.AppendChild(signature);

            //Optional
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            //AddPrefixToAllNodes(xmlDigitalSignature);

            //xmlDigitalSignature.= "http://www.w3.org/2000/09/xmldsig#";

            //XmlElement root = (XmlElement)doc.GetElementsByTagName("soapenv:Envelope")[0];
            //string query = string.Format("//*[@Id='{0}']", "IDH"); //Search the Header tag to add signature
            XmlElement header = (XmlElement)doc.GetElementsByTagName("s:Header")[0];

            var security = doc.CreateElement("wsse", "Security", WSSE);
            security.AppendChild(xmlDigitalSignature);
            AddPrefixToAllNodes(security);

            header.AppendChild(security);

            AppendKeyInfo(doc, cert);


            return doc;
        }

        static void AppendKeyInfo(XmlDocument doc, X509Certificate2 cert)
        {
            var keyInfo = doc.CreateElement("ds", "KeyInfo", DS);
            var securityTokenReference = doc.CreateElement("wsse", "SecurityTokenReference", WSSE);
            var X509Data = doc.CreateElement("ds", "X509Data", DS);
            var X509IssuerSerial = doc.CreateElement("ds", "X509IssuerSerial", DS);
            var X509IssuerName = doc.CreateElement("ds", "X509IssuerName", DS);
            X509IssuerName.InnerText = cert.IssuerName.Name;
            var X509SerialNumber = doc.CreateElement("ds", "X509SerialNumber", DS);
            X509SerialNumber.InnerText = cert.SerialNumber;


            keyInfo.AppendChild(securityTokenReference);
            securityTokenReference.AppendChild(X509Data);
            X509Data.AppendChild(X509IssuerSerial);
            X509IssuerSerial.AppendChild(X509IssuerName);
            X509IssuerSerial.AppendChild(X509SerialNumber);

            XmlElement signature = (XmlElement)doc.GetElementsByTagName("ds:Signature")[0];
            signature.AppendChild(keyInfo);
        }

        static void AddPrefixToAllNodes(XmlNode xmlDigitalSignature)
        {
            foreach (var childNode in xmlDigitalSignature.ChildNodes)
            {

                XmlNode xmlNode = childNode as XmlNode;
                if (xmlNode.Name == "Security")
                {
                    xmlNode.Prefix = "wsse";
                }
                if (xmlNode.Name == "InclusiveNamespaces")
                {
                    xmlNode.Prefix = "ec";
                    return;
                }
                else
                {
                    xmlNode.Prefix = "ds";
                }
                AddPrefixToAllNodes(xmlNode);
            }
        }
    }
}
