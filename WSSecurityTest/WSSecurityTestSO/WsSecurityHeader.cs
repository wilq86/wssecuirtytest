﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WSSecurityTestSO
{
    public sealed class WsSecurityHeader : MessageHeader
    {
        public override bool MustUnderstand => true;

        public override string Name => "Security";

        public const string SoapEnvelopeNamespace = "http://schemas.xmlsoap.org/soap/envelope/";
        public const string WsseUtilityNamespaceUrl = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
        public const string WsseNamespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

        public override string Namespace => WsseNamespace;

        protected override void OnWriteStartHeader(XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            writer.WriteStartElement("wsse", Name, Namespace);
            writer.WriteAttributeString("s", "mustUnderstand", SoapEnvelopeNamespace, "1");

            writer.WriteXmlnsAttribute("wsse", Namespace);
            writer.WriteXmlnsAttribute("wsu", WsseUtilityNamespaceUrl);
        }

        protected override void OnWriteHeaderContents(XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            // Timestamp
            writer.WriteStartElement("wsu", "Timestamp", WsseUtilityNamespaceUrl);

            writer.WriteAttributeString("wsu", "Id", WsseUtilityNamespaceUrl, "ws-security-timestamp");

            writer.WriteStartElement("wsu", "Created", WsseUtilityNamespaceUrl);
            writer.WriteValue(DateTimeOffset.Now.ToString("o"));
            writer.WriteEndElement();

            writer.WriteStartElement("wsu", "Expires", WsseUtilityNamespaceUrl);
            writer.WriteValue(DateTimeOffset.Now.AddMinutes(120).ToString("o"));
            writer.WriteEndElement();

            writer.WriteEndElement(); // Timestamp
        }
    }
}
