﻿// See https://aka.ms/new-console-template for more information

using ServiceReference1;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using WSSecurityTestSO;

//https://stackoverflow.com/questions/60675836/sign-soap-1-1-body-with-net-core-3-1

var certificate = new X509Certificate2("apples.pfx", "apples");
var endPoint = new EndpointAddress("https://www.dneonline.com/calculator.asmx");

var binding = new BasicHttpsBinding();
//binding.Security.Mode = BasicHttpsSecurityMode.Transport;

var client = new CalculatorSoapClient(binding, endPoint);

// Configure ws-security signing
client.ChannelFactory.Endpoint.EndpointBehaviors.Add(new WsSecurityHeaderBehavior(certificate));

var a = await client.AddAsync(1, 1);

Console.WriteLine("Hello, World!");
