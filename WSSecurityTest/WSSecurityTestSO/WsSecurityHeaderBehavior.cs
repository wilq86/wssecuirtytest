﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace WSSecurityTestSO
{
    public sealed class WsSecurityHeaderBehavior : IEndpointBehavior
    {
        public X509Certificate2 X509Certificate { get; }

        public WsSecurityHeaderBehavior() { }

        public WsSecurityHeaderBehavior(X509Certificate2 cert)
        {
            X509Certificate = cert;
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            var inspector = new WsSecurityMessageInspector(X509Certificate);
            clientRuntime.ClientMessageInspectors.Add(inspector);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }

        public void Validate(ServiceEndpoint endpoint) { }
    }
}
